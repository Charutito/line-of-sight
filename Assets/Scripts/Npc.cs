﻿using UnityEngine;
using System.Collections;

public class Npc : MonoBehaviour {

    public GameObject target;
    public float viewAngle;
    public float viewDistance;
    
    private Vector3 _dirToTarget;
    private float _angleToTarget;
    private float _distanceToTarget;
    private bool _targetInSight;

    
	void Update () {
        /*
        Primero calculamos que cumpla con los requisitos de distancia y ángulo.
        Es decir, que este dentro del campo de visión sin contar obstáculos.
        */

        _dirToTarget = target.transform.position - transform.position; //Siempre la dirección desde un punto a otro es: Posición Final - Posición Inicial

        _angleToTarget = Vector3.Angle(transform.forward, _dirToTarget); //Vector3.Angle nos da el ángulo entre dos direcciones

        _distanceToTarget = Vector3.Distance(transform.position, target.transform.position); //Vector3.Distance nos da la distancia entre dos posiciones


        if (_angleToTarget <= viewAngle && _distanceToTarget <= viewDistance)
        {
            /*
            Una vez que descartamos las primeras posibilidades, vamos a utilizar un raycast.
            */
            RaycastHit[] rch;
            _targetInSight = true;
            rch = Physics.RaycastAll(transform.position, _dirToTarget, _distanceToTarget);

            for (int i = 0; i < rch.Length; i++)
            {
                RaycastHit hit = rch[i];
                if (hit.collider.gameObject.layer == Layers.WALL)
                    _targetInSight = false;
            }
        }
        else //Si no se cumplieron las condiciones
            _targetInSight = false;
	}

    void OnDrawGizmos()
    {
        /*
        Dibujamos una línea desde el NPC hasta el enemigo.
        Va a ser de color verde si lo esta viendo, roja sino.
        */
        if (_targetInSight)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, target.transform.position);

        /*
        Dibujamos los límites del campo de visión.
        */
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, viewDistance);

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + (transform.forward * viewDistance));

        Vector3 rightLimit = Quaternion.AngleAxis(viewAngle, transform.up) * transform.forward;
        Gizmos.DrawLine(transform.position, transform.position + (rightLimit * viewDistance));

        Vector3 leftLimit = Quaternion.AngleAxis(-viewAngle, transform.up) * transform.forward;
        Gizmos.DrawLine(transform.position, transform.position + (leftLimit * viewDistance));
    }
}
